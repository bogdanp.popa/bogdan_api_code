import requests
import pytest
from base64 import b64encode
import lorem
import time

# GLOBAL VARIABLES
username = 'editor'
username_c = 'commentary'
password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
blog_url = 'https://gaworski.net'
posts_endpoint_url = f"{blog_url}/wp-json/wp/v2/posts"
comments_endpoint_url = f"{blog_url}/wp-json/wp/v2/comments"
token = b64encode(f"{username}:{password}".encode('utf-8')).decode("ascii")
token2 = b64encode(f"{username_c}:{password}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def headers():
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Basic {token}"
    }
    return headers


@pytest.fixture(scope='module')
def created_article(headers):
    timestamp = int(time.time())
    article_data = {
        "title": f"This is new post {timestamp}",
        "content": lorem.paragraph(),
        "status": "publish"
    }
    response = requests.post(posts_endpoint_url, headers=headers, json=article_data)
    assert response.status_code == 201, f"Failed to create article: {response.text}"
    article_id = response.json()["id"]
    print(f"Article created successfully with ID: {article_id}")
    return article_id


def test_add_commentary_comment(headers, created_article):
    comment_data = {
        "post": created_article,
        "author_name": username_c,
        "author_email": "commentary@example.com",
        "content": "This is a commentary comment on the article."
    }
    response = requests.post(comments_endpoint_url, headers=headers, json=comment_data)
    assert response.status_code == 201, f"Failed to add commentary comment: {response.text}"
    print("Commentary comment added successfully.")


def test_add_editor_reply(headers, created_article):

    comment_data = {
        "post": created_article,
        "author_name": username_c,
        "author_email": "commentary@example.com",
        "content": "This is a commentary comment on the article_new"
    }
    comment_response = requests.post(comments_endpoint_url, headers=headers, json=comment_data)
    assert comment_response.status_code == 201, f"Failed to add commentary comment: {comment_response.text}"
    comment_id = comment_response.json()["id"]

    reply_data = {
        "post": created_article,
        "parent": comment_id,
        "author_name": username,
        "author_email": "editor@example.com",
        "content": "This is an editor's reply to the commentary comment."
    }
    reply_response = requests.post(comments_endpoint_url, headers=headers, json=reply_data)
    assert reply_response.status_code == 201, f"Failed to add editor's reply: {reply_response.text}"
    print("Editor's reply added successfully.")
